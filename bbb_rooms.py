#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2020 continuity.space SRL
#                    Marco Marinello <mmaridev@continuity.space>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import psycopg2, json

GL_DB_USER = "postgres"
GL_DB_PASS = ""

def get_all_rooms():
    try:
        conn = psycopg2.connect(
            "dbname='greenlight_production'"
            " user='{}' host='127.0.0.1' password='{}'".format(
                GL_DB_USER, GL_DB_PASS
            )
        )
    except Exception as e:
        # Unable to connect to the db
        exit(1)
    cur = conn.cursor()
    cur.execute("""SELECT id, email from users""")
    users = dict(cur.fetchall())
    cur = conn.cursor()
    cur.execute("""SELECT user_id, bbb_id, name from rooms""")
    rooms = cur.fetchall()
    out = {}
    for room in rooms:
        out[room[1]] = "{} ({})".format(room[2], users[room[0]])
    return out


if __name__ == "__main__":
    data = get_all_rooms()
    out = []
    for key in data:
        out.append({
            "{#BBBID}": key,
            "{#NAME}": data[key]
        })
    print(json.dumps({"data": out}))
