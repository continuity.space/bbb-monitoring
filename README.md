# BBB Monitoring
## Zabbix scripts to monitor a BBB instance

Use the `bbb-monitoring.yaml` playbook to install.

```
ansible-playbook -u root -i YOU.R.IP.ADD, bbb-monitoring.yaml
```
