#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2020 Marco Marinello <mmarinello@fuss.bz.it>
# Copyright (c) 2020 continuity.space SRL
#                    Marco Marinello <mmaridev@continuity.space>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, requests
from lxml import etree
import bbb_rooms

SUM = "GETMEETINGS_SUM_HERE"
MEETING_ID = sys.argv[1]
QUERY = sys.argv[2]


ct = requests.get(
    "http://127.0.0.1:8090/bigbluebutton/api/getMeetings?checksum={}".format(
        SUM
    )
).text

root = etree.fromstring(ct).getroottree()


def get_top(root):
    out = []
    rooms = bbb_rooms.get_all_rooms()
    for this_meeting in root.xpath("//meetings//meeting"):
        this = [-1, -1, -1, ""]
        try:
            this[0] = int(this_meeting.xpath("//participantCount")[0].text)
            this[1] = int(this_meeting.xpath("//voiceParticipantCount")[0].text)
            this[2] = int(this_meeting.xpath("//videoCount")[0].text)
            this[3] = this_meeting.xpath("//meetingID")[0].text
        except:
            pass
        out.append(this)
    for i in sorted(out, key=lambda x: x[0], reverse=True):
        print(*i[:-1], rooms[i[-1]])


if MEETING_ID == "all":
    if QUERY == "top":
        get_top(root)
        exit()
    try:
        checkattr = {
            "webcams": "videoCount",
            "users": "participantCount",
            "mics": "voiceParticipantCount",
        }[QUERY]
    except KeyError:
        print("ERR")
        exit()
    total = 0
    for meeting in root.xpath("//meetings//meeting"):
        try:
            total += int(meeting.xpath("//{}".format(checkattr))[0].text)
        except:
            pass
    print(total)


else:
    this_meeting = root.xpath(
        "//meetings//meeting//meetingID[text() = '{}']".format(MEETING_ID)
    )
    if not this_meeting:
        print("0")
        exit()

    this_meeting = this_meeting[0]

    if QUERY == "webcams":
        print(this_meeting.xpath("//videoCount")[0].text)

    elif QUERY == "users":
        print(this_meeting.xpath("//participantCount")[0].text)

    elif QUERY == "mics":
        print(this_meeting.xpath("//voiceParticipantCount")[0].text)
